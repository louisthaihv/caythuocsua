$(document).ready(function() {
    //Login form
    $('a.login-window').click(function() {

        //lấy giá trị thuộc tính href - chính là phần tử "#login-box"
        var loginBox = $(this).attr('href');
        //cho hiện hộp đăng nhập trong 300ms
        $(loginBox).fadeIn("slow");

        // thêm phần tử id="over" vào cuối thẻ body
        $('body').append('<div id="over"></div>');
        $('#over').fadeIn(300);

        $('#btn-login').click(function(){
            if($('#username').val() == "" || $('#password').val() == ""){
                alert('dien day du thong tin vao');
                if($('#username').val() == ""){$('#username').focus();}
                else{$('#password').focus();}
                return false;
            }else{
                return true;            
            }
        });
    });

//Register form

    $("#register-form").validate({
        rules : {
            firstName : {
                required : true,
                minlength : 3
                
            },
            lastName : {
                required : true,
                minlength : 3

            },
            password1: {
                required : true,
                minlength: 6
            },
            re_Password: {
                required : true,
                equalTo : "#password1"
            },
            email: {
                required:true,
                email: true,
               
            }

        },

        messages: {
            firstName : {
                required : "Vui lòng nhập first name",
                minlength : "Username phải nhập 3 kí tự trở lên",
                remote : "User name đã tồn tại"
            },
            password1 : {
                required : "Vui lòng nhập password",
                minlength : "Mật khẩu phải nhập 6 kí tự trở lên"
            },
            re_Password : {
                required : "Vui lòng xác nhận password",
                equalTo : "Mật khẩu xác nhận không đúng"
            },
            email: {
                required:"Vui long nhập email",
                email: "Không đúng định dạng email",
                remote : "Email đã tồn tại"
            },

            lastName: {
                required:"Vui long nhập lastName",
            } 

    }
    });

    $('a.register-window').click(function(){
        var registerBox = $(this).attr('href');

        //cho hiện hộp đăng nhập trong 300ms
        $(registerBox).fadeIn("slow");

        // thêm phần tử id="over" vào cuối thẻ body
        $('body').append('<div id="over"></div>');
        $('#over').fadeIn(300);
    });

    // khi click đóng hộp thoại
    $(document).on('click', "a.close, #over", function() { 
        $('#over, .login').fadeOut(300 , function() {
            $('#over').remove();  
        }); 
    });
});


