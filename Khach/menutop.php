
<div id="smoothmenu1" class="ddsmoothmenu">
<ul>
<li><a href="#">Cây Thuốc</a></li>
<li><a href="#">Món Ăn - Bài Thuốc</a></li>
<li><a href="#">Mẹo Hay - Chữa Bệnh</a></li>
<li><a href="#">Cẩm Nang - Sức Khỏe</a></li>
<li><a  href="#register-box" class="register-window">Đăng Ký</a></li>
<li><a href="#login-box" class="login-window">Đăng nhập</a></li>
</ul>
<br style="clear: left" />
</div>
<div style="clear:both"></div>

//Content of Login Form
<div id="login-box" class="login">
           <p class="login_title"> Đăng nhập</p>
            <a href="#" class="close"><img src="images/close.png" class="img-close" title="CloseWindow" alt="Close" /></a>
            <form method="post" class="login-content" action="dangnhap.php">
                <label class="username">
                    <span>Tên hoặc email</span>
                    <input name="tendangnhap" value="" type="text" autocomplete="on" placeholder="Username">
                </label>
                <label class="password">
                    <span>Mật khẩu</span>
                    <input name="matkhau" value="" type="password" placeholder="Password">
                </label>
                <button class="button submit-button" type="submit" id ="btn-login" name="dangnhap">Đăng nhập</button>
                <p>
                 <a href="#">Quên mật khẩu?</a>
                </p>
            </form>
</div>

//content of Register window
 <div id="register-box" class="login">
    <p class="login_title"> Đăng Ký</p>
    <a href="#" class="close"><img src="images/close.png" class="img-close" title="CloseWindow" alt="Close" /></a>
    <form action="register.php" method="post" id="register-form" novalidate="novalidate">
        <h2>User Registration</h2>
        <div id="form-content">
            <fieldset>

                <div class="fieldgroup">
                    <input type="text" name="firstName" placeholder = 'first-name'/>
                </div>

                <div class="fieldgroup">
                    <input type="text" name="lastName" placeholder = 'last-name'/>
                </div>

                <div class="fieldgroup">
                    <input type="email" name="email" id ="email" placeholder = 'email'/>
                </div>

                <div class="fieldgroup">
                    <input type="text" name="address" placeholder = 'address'/>
                </div>

                <div class="fieldgroup">
                    <input type="text" name="username1" id ="username1" placeholder = 'username'/>
                </div>

                <div class="fieldgroup">
                    <input type="password" name="password1" id = "password1" placeholder = 'password'/>
                </div>

                <div class="fieldgroup">
                    <input type="password" name="re_Password" id = "re_Password" placeholder = 're_Password'/>
                </div>

                <div class="fieldgroup">
                  <input type="submit" value="Register" class="submit" id="btnRegister" name="btRegister"/>
                </div>

            </fieldset>
        </div>

        <div class="fieldgroup">
            <p>Already registered? <a href="/login">Sign in</a>.</p>
        </div>
    </form>
</div>